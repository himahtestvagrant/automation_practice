package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageInitialisation {
    public HomePage(WebDriver driver){
        super(driver);

    }
    @FindBy(linkText = "Sign in")
    WebElement homePageLink;
    public void clickHomePageLink(){
        homePageLink.click();

    }
}
