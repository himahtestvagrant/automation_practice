package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignLog  extends PageInitialisation{
    public SignLog(WebDriver driver){
        super(driver);

    }
    @FindBy(id= "email_create")
    WebElement signUpEmail;
    @FindBy(id="SubmitCreate")
    WebElement signUpButton;

    @FindBy(id="email")
    WebElement logInEmail;
    @FindBy(id="passwd")
    WebElement logInPass;
    @FindBy(id="SubmitLogin")
    WebElement logInSubmitLogin;

    public void enterSignupEmail(String email){
        signUpEmail.sendKeys(email);

    }
    public void pressSignupButton(){
        signUpButton.click();

    }
    public void enterLoginEmail(String email){
        logInEmail.sendKeys(email);
    }
    public void enterLoginPassword(String password) {
        logInPass.sendKeys(password);

    }
    public void  pressLoginButton(){
        logInSubmitLogin.click();
    }


}
