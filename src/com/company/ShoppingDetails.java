package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ShoppingDetails extends PageInitialisation{
    public ShoppingDetails(WebDriver driver){
        super(driver);
    }
    @FindBy(id="quantity_wanted")
    WebElement quantity;

    @FindBy(xpath="//*[@id='color_13']")
    WebElement colour;

    @FindBy(id="group_1")
    WebElement size;

    @FindBy(xpath="//*[@id='add_to_cart']/button")
    WebElement  order;

    public void enterQuantity(String qty){
        quantity.clear();
        quantity.sendKeys(qty);
    }
    public void pressColour(){
        colour.click();

    }

    public void selectSize(String size1){
        Select s= new Select(size);
        s.selectByVisibleText(size1);
    }

    public void  addCart(){
        order.click();
    }
}
