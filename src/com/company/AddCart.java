package com.company;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class AddCart {
    public static WebDriver driver;
    public static void main(String[] args) throws IOException, ParseException {
        FileInputStream fileInput = new FileInputStream("/Users/himaheggade/IdeaProjects/website/src/com/company/config.properties");
        Properties prop = new Properties();
        prop.load(fileInput);
        if (prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver=new ChromeDriver();

        }
        else{
            WebDriverManager.firefoxdriver().setup();
            driver=new FirefoxDriver();
        }
        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("/Users/himaheggade/IdeaProjects/website/src/com/company/data.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignInData");

        JSONObject userDetail = (JSONObject) array.get(0);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(prop.getProperty("url"));

        HomePage home = new HomePage(driver);
        home.clickHomePageLink();

        SignLog s1 = new SignLog(driver);
        s1.enterLoginEmail((String)userDetail.get("email"));
        s1.enterLoginPassword((String)userDetail.get("passwd"));
        s1.pressLoginButton();


        CartDetails cd = new CartDetails(driver);
        cd.pressWomenCategory();
        cd.selectProduct();

        ShoppingDetails sh= new ShoppingDetails(driver);
        sh.enterQuantity("2");
        sh.selectSize("M");
        sh.pressColour();

        sh.addCart();


        /*

        driver.findElement(By.id("quantity_wanted")).clear();
        driver.findElement(By.id("quantity_wanted")).sendKeys("3");
        driver.findElement(By.xpath("//*[@id=\"color_14\"]")).click();
        Select sizeSelector= new Select(driver.findElement(By.id("group_1")));
        sizeSelector.selectByVisibleText("M");
        driver.findElement(By.xpath("//*[@id=\"add_to_cart\"]/button")).click();
        driver.quit();*/
    }
}
