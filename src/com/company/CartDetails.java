package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class CartDetails extends PageInitialisation {
    WebDriver driver;
    public CartDetails(WebDriver driver) {
        super(driver);
        this.driver=driver;

    }


    @FindBy(linkText="WOMEN")
    WebElement women;

    @FindBy(xpath = "//*[@id='center_column']/ul/li[3]/div/div[1]/div/a[1]/img")
    WebElement img;
    @FindBy(xpath = "//*[@id='center_column']/ul/li[3]/div/div[2]/div[2]/a[2]")
    WebElement MoreBtn;




    public void pressWomenCategory(){
        women.click();
    }

    public void selectProduct(){
        Actions actions=new Actions(driver);
        actions.moveToElement(img).moveToElement(MoreBtn).click().perform();

    }




}