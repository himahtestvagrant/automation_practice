package com.company;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Singup {
    public static WebDriver driver;
    public static void main(String[] args) throws IOException, ParseException {
        FileInputStream fileInput = new FileInputStream("/Users/himaheggade/IdeaProjects/website/src/com/company/config.properties");
        Properties prop = new Properties();
        prop.load(fileInput);
        if (prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver=new ChromeDriver();

        }
        else{
            WebDriverManager.firefoxdriver().setup();
            driver=new FirefoxDriver();
        }
        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("/Users/himaheggade/IdeaProjects/website/src/com/company/data.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignUpData");

        JSONObject userDetail = (JSONObject) array.get(0);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(prop.getProperty("url"));

        HomePage home = new HomePage(driver);
        home.clickHomePageLink();

        SignLog s1 = new SignLog(driver);
        s1.enterSignupEmail((String)userDetail.get("email_create"));
        s1.pressSignupButton();

        UserDetails user = new UserDetails(driver);
        user.pressGender();
        user.setCustomerFirstName((String)userDetail.get("customer_firstname"));
        user.setCustomerLastName((String)userDetail.get("customer_lastname"));
        user.setPassword((String)userDetail.get("passwd"));
        user.setFirstName((String)userDetail.get("firstname"));
        user.setLastName((String)userDetail.get("lastname"));
        user.setAddress((String)userDetail.get("address1"));
        user.setCity((String)userDetail.get("city"));
        user.pickState("2");
        user.setPostalCode((String)userDetail.get("postcode"));
        user.pickCountry("United States");
        user.setPhoneNumber((String)userDetail.get("phone_mobile"));
        user.setAlias((String)userDetail.get("alias"));
        user.pressSignUpButton();






    }
}
