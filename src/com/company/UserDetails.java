package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class UserDetails extends PageInitialisation{
    public UserDetails(WebDriver driver){
        super(driver);

    }
    @FindBy(id="id_gender2")
    WebElement gender;

    @FindBy(id="customer_firstname")
    WebElement customerFirstName;

    @FindBy(id="customer_lastname")
    WebElement customerLastName;

    @FindBy(id="passwd")
    WebElement passwd;

    @FindBy(id="firstname")
    WebElement firstname;

    @FindBy(id="lastname")
    WebElement lastname;

    @FindBy(id="company")
    WebElement company;

    @FindBy(id="address1")
    WebElement address1;

    @FindBy(id="city")
    WebElement city;

    @FindBy(id="id_state")
    WebElement state;

    @FindBy(id="postcode")
    WebElement postcode;

    @FindBy(id="id_country")
    WebElement country;

    @FindBy(id="phone_mobile")
    WebElement phoneMobile;

    @FindBy(id="alias")
    WebElement alias;

    @FindBy(id="submitAccount")
    WebElement submitAccount;

    public void pressGender(){
        gender.click();

    }
    public void setCustomerFirstName(String name){
        customerFirstName.sendKeys(name);
    }

    public void setCustomerLastName(String name){
        customerLastName.sendKeys(name);
    }
    public void setPassword(String password){
        passwd.sendKeys(password);
    }
    public void setFirstName(String name){
        firstname.sendKeys(name);
    }
    public void setLastName(String name){
        lastname.sendKeys(name);
    }
    public void setAddress(String address){
        address1.sendKeys(address);
    }
    public void setCity(String city1){
        city.sendKeys(city1);
    }
    public void pickState(String stateValue){
        Select state1=new Select(state);
        state1.selectByValue(stateValue);
    }
    public void setPostalCode(String code){
        postcode.sendKeys(code);
    }
    public void pickCountry(String countryName){
        Select country1=new Select(country);
        country1.selectByVisibleText(countryName);
    }
    public void setPhoneNumber(String phNumber){
        phoneMobile.sendKeys(phNumber);
    }
    public void setAlias(String alias1){
        alias.sendKeys(alias1);
    }
    public void pressSignUpButton(){
        submitAccount.click();
    }


}
